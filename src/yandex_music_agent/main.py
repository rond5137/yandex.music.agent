import asyncio
import configparser
import os
import re
from pathlib import Path

import argparse

from yandex_music_agent import auth
from yandex_music_agent.agent import Depth, YandexMusicAgent
from yandex_music_agent.api import YandexMusicApi
from yandex_music_agent.common import enum_print


def app_dir() -> str:
    return os.path.join(Path.home(), "YandexMusicAgent")


class OutputDir:

    def __init__(self):
        self._storage = os.path.join(app_dir(), ".output")
        if os.path.exists(self._storage):
            with open(self._storage, "rt") as file:
                self._value = file.read()
        else:
            self._value = os.path.join(Path.home(), "Music")

    @property
    def value(self) -> str:
        return self._value

    @value.setter
    def value(self, value: str):
        with open(self._storage, "wt") as file:
            file.write(value)
        self._value = value


def resolve_cookie() -> str:
    base_dir = app_dir()
    os.makedirs(base_dir, exist_ok=True)
    cookie_file = os.path.join(base_dir, ".cookie")
    if os.path.exists(cookie_file):
        with open(cookie_file, "rt") as file:
            return file.read()
    credentials_file = os.path.join(base_dir, ".credentials")
    if os.path.exists(credentials_file):
        config = configparser.ConfigParser()
        config.read(credentials_file)
        login = config["yandex"]["login"]
        password = config["yandex"]["password"]
    else:
        raise Exception(f"""Create \"{credentials_file}\" with content

[yandex]
login=<user_login>
password=<user_password>
""")
    cookie = auth.resolve_cookie(login, password)
    with open(cookie_file, "wt") as file:
        file.write(cookie)
    return cookie


async def amain():
    output = OutputDir()
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--artist", help="Artist ID")
    parser.add_argument("-o", "--output", default=output.value,
                        help=f"Output directory, current: {output.value}")
    parser.add_argument("-d", "--depth", default=0, type=int,
                        help=f"Exists files check depth, {enum_print(Depth)}")
    args = parser.parse_args()
    if args.output:
        output.value = args.output
    cookie = resolve_cookie()
    api = YandexMusicApi(cookie)
    agent = YandexMusicAgent(api, output.value)
    if args.artist:
        artist = await api.get_artist(args.artist)
        await agent.download_artist(artist, args.depth)
    else:
        email = re.compile(".*?yandex_login=(.*?);.*?", re.M).match(cookie).group(1)
        await agent.download_favorites(email, args.depth)


def main():
    asyncio.run(amain())


if __name__ == "__main__":
    main()
