from collections import namedtuple


class Artist(namedtuple("Artist", ["id", "title"])):
    pass


class Album(namedtuple("Album", ["id", "title", "year", "cover"])):
    pass


class Track(namedtuple("Track", ["album_id", "id", "title", "num"])):
    pass
