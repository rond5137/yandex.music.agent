import os
from enum import IntEnum

import aiohttp
import mutagen
import mutagen.id3
from aiofile import AIOFile
from mutagen.easyid3 import EasyID3
from tqdm import tqdm

from yandex_music_agent.api import YandexMusicApi
from yandex_music_agent.data import Artist


class Depth(IntEnum):
    NORMAL = 0
    ALBUMS = 1
    TRACKS = 2


def normalize(name: str) -> str:
    return name.replace("/", "-")


class YandexMusicAgent:

    def __init__(self, api: YandexMusicApi, target_dir: str):
        self.api = api
        self.target_dir = target_dir

    @classmethod
    async def download_file(cls, url: str, filename: str):
        async with aiohttp.ClientSession() as session:
            async with session.request(method="GET", url=url) as response:
                data = await response.read()
                async with AIOFile(filename, "wb") as afp:
                    await afp.write(data)
                    await afp.fsync()

    @classmethod
    def write_tags(cls, filename: str, tags: dict):
        try:
            meta = EasyID3(filename)
        except mutagen.id3.ID3NoHeaderError:
            meta = mutagen.File(filename, easy=True)
            meta.add_tags()
        for k, v in tags.items():
            meta[k] = v
        meta.save(filename, v1=2)

    async def download_artist(self, artist: Artist, depth: Depth = Depth.NORMAL):
        artist_progress = tqdm(total=0, desc=artist.title, position=1, ascii=True)
        albums = await self.api.get_artist_albums(artist.id)
        artist_progress.total = len(albums)
        artist_progress.refresh()
        for album in albums:
            album_dir = os.path.join(self.target_dir, normalize(artist.title), f"{album.year} - {normalize(album.title)}")
            if depth < Depth.ALBUMS and os.path.exists(album_dir):
                artist_progress.update()
                continue
            album_progress = tqdm(total=0, desc=f"> {album.title}", position=0, ascii=True)
            tracks = await self.api.get_album_tracks(album.id)
            album_progress.total = len(tracks)
            album_progress.refresh()
            os.makedirs(album_dir, exist_ok=True)
            if album.cover:
                album_progress.total += 1
                cover_filename = os.path.join(album_dir, "cover.jpg")
                if not os.path.exists(cover_filename):
                    await self.download_file(album.cover, cover_filename)
                album_progress.update()
            for track in tracks:
                target_filename = os.path.join(album_dir, f"{track.num:02d}. {normalize(track.title)}.mp3")
                if depth >= Depth.TRACKS or not os.path.exists(target_filename):
                    url = await self.api.get_track_url(track.album_id, track.id)
                    await self.download_file(url, target_filename)
                    self.write_tags(target_filename, {
                        "title": track.title,
                        "tracknumber": str(track.num),
                        "artist": artist.title,
                        "album": album.title,
                        "date": str(album.year),
                    })
                album_progress.update()
            album_progress.close()
            artist_progress.update()
        artist_progress.close()

    async def download_favorites(self, email: str, depth: Depth = Depth.NORMAL):
        artists = await self.api.get_favorite_artists(email)
        for artist in artists:
            await self.download_artist(artist, depth)
